var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Division
var Division = new Schema({
  name: {
	type : String
  },
  Head: {
	type : Schema.Types.ObjectId
  },
},{
    collection: 'divisions'
});

module.exports = mongoose.model('Division', Division);