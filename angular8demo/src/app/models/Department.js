var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Department
var Department = new Schema({
  name: {
	type : String
  },
  Head: {
	type : Schema.Types.ObjectId
  },
},{
    collection: 'departments'
});

module.exports = mongoose.model('Department', Department);