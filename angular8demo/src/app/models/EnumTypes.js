
// enum type CompanyType
export let CompanyType = {
	S_Corp:"S_Corp",
	LLC:"LLC",
	C_Corp:"C_Corp",
}

// enum type EmploymentType
export let EmploymentType = {
	Manager:"Manager",
	Board Member:"Board Member",
	Team Lead:"Team Lead",
	Consultant:"Consultant",
	Vice President:"Vice President",
	Sr. Mananager:"Sr. Mananager",
	Director:"Director",
	Engineer:"Engineer",
}

// enum type Industry
export let Industry = {
	Bank:"Bank",
	Insurance:"Insurance",
	Manufacturer:"Manufacturer",
	Technology:"Technology",
	Health:"Health",
	Financial:"Financial",
}
