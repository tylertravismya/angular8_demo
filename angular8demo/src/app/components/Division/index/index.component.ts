import { DivisionService } from '../../../services/Division.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Division } from '../../../models/Division';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexDivisionComponent implements OnInit {

  divisions: any;

  constructor(private http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: DivisionService) {}

  ngOnInit() {
    this.getDivisions();
  }

  getDivisions() {
    this.service.getDivisions().subscribe(res => {
      this.divisions = res;
    });
  }

  deleteDivision(id) {
    this.service.deleteDivision(id)
		.then(success => 
			{
				this.router.navigateByUrl('/', {skipLocationChange: false}).then(()=>
							this.router.navigate(['indexDivision']));
			});  }
}
